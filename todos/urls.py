from django.urls import path

# from django.contrib.auth import views as auth_views

from todos.views import TodoListView, TodoListDetailView

urlpatterns = [
    path("", TodoListView.as_view(), name="todo_list_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todo_list_detail"),
]
