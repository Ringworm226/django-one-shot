from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from todos.models import TodoList, TodoItem

# Create your views here.


class TodoListView(ListView):
    model = TodoList
    template_name = "list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "detail.html"
